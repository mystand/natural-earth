# Readme

![natural-earth.png](https://bitbucket.org/repo/Ap794g/images/2966407944-natural-earth.png)

### Install Gerardus

```shell
npm install -g gerardus
```

### Start simple HTTP server

```shell
python -m SimpleHTTPServer 8000
```

### Run Gerardus

```shell
gerardus --verbose natural-earth.tm2source.mbtiles
```

### Open browser

```shell
open http://localhost:8000
```
